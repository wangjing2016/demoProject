var indexLoad = (function(){
    'use strict';
    // 初始化函数
    var init = function(){
        jqueryClick();
        protoClick();
        introduction();
    };
    // 小玩意儿
    var jqueryClick = function(){
        var jqueryBtn = document.getElementById("jquery");
        jqueryBtn.onclick = function(){
            window.location = "indexJquery.html"
        }
    };
    // 框架demo
    var protoClick = function(){
        var protoBtn = document.getElementById("protoType");
        protoBtn.onclick = function(){
            window.location = "indexProto.html"
        }
    };
    // 个人简历
    var introduction = function(){
        var introBtn = document.getElementById("introduction");
        introBtn.onclick = function(){
            window.location = "personal.html"
        }
    };
    return {
        init : init
    }
})();